// Copyright 2015 (c) Ron Ostafichuk
#include <iostream>
#include <string>
#include <time.h>

#define MAX_BARCODES 80
class Locator
{
private:
	double dPositionNS; // relative coordinates in yard
	double dPositionEW;

	int nNumBarcodesFound;
	int nBarValue[MAX_BARCODES]; // the Barcode Value identified (1-...)
	double aAngle[MAX_BARCODES]; // this array contains angles to each barcode in degrees
	double aDepth[MAX_BARCODES]; // contains estimate of distance to the barcode (meters) based on the height in pixels of the total barcode and the known barcode size of ? meters

public:
  Locator()
  {
	  dPositionNS=0;
	  dPositionEW=0;
	  nNumBarcodesFound=0;
  }; // constructor, set up member variables

  ~Locator()
  {

  }; // destructor, free any memory allocated by this class

  int ProcessImage( char* pImageData, int nWidth, int nHeight, long nImageSize); // calc position estimate based on the image contents, return number of barcodes in image, and alter the latest position information

  // function for getting the time in ms
  unsigned long getTime_ms()
  {
	long time_ms;
	struct timespec t;
	clock_gettime(CLOCK_REALTIME, &t); // This function is using the Realtime library

	time_ms = t.tv_sec*1000 + (t.tv_nsec / 1.0e6); // Convert nanoseconds to milliseconds
	return time_ms;
  }

};
