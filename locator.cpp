#include <iostream>
#include <time.h>
#include <stdlib.h>
#include <math.h>
#include "locator.h"

using namespace std;

int Locator::ProcessImage( char* pImageData, int nWidth, int nHeight, long nImageSize)
{
	unsigned long nStart_ms = this->getTime_ms();

	bool bShowOutput = false;
	if( bShowOutput )
	    cout << "====================\r\nProcessImage called (*,"<<nWidth<<","<<nHeight<<","<<nImageSize<<"\r\n";

	// calc position estimate based on the image contents, return number of barcodes in image, and alter the latest position information
	this->nNumBarcodesFound = 0;

	// prepare a place to store temporary results
	int *nCodesFound = new int[nWidth];
	int *nHeightFound = new int[nWidth];
	for( int w=1;w<nWidth;w++)
	{
		nCodesFound[w]=0;
		nHeightFound[w]=0;
	}
	int nValleyHList[100]; // max 100 valleys, save the h value for each valley so we can look for a pattern
	int nMaxValleys = 100;

	// process all vertical lines hoping to find a barcode
	int nWidthInBytes = nWidth*3; // optimization for rgb
	for( int w = 0 ; w< nWidth ; w++)
	{
		// for each column of pixels, look for a vertical barcode
		int nMode = 0; // 0= looking for first valley \_/
		int nGoodPeriodCount = 0;
		int nPrevPeriod = 0;
		int nPeriod=0;
		int nPrevTransition = 0;
		int nFirstTransition = 0;
		int nMinRed = 255; // min and max red within the column and within the barcode
		int nMaxRed = 0;
		// first do an averging filter to make the data a little less noisy
		for( int h=2;h<nHeight-2;h++)
		{
			pImageData[h] = ((int)pImageData[h-2] + (int)pImageData[h-1] + (int)pImageData[h] + (int)pImageData[h+1] + (int)pImageData[h+2]) / 5;
		}

		// init the valley list
		for( int nInit=0; nInit< nMaxValleys ; nInit++)
			nValleyHList[nInit]=0;
		int nHIdx = 0;
		// now build a list of valleys that are at least 10 deep (in red intensity levels)
		int nRedBeforeDrop = 255;
		int nValley = 30;
		int nDirection = 0; // direction of travel
		int nMin = 255;
		int nMinIdx = 0;
		for( int h=0;h<nHeight-1;h++)
		{
			int nRed0 = pImageData[w*3+h*nWidthInBytes];
			int nRed1 = pImageData[w*3+(h+1)*nWidthInBytes];

			if( nDirection >= 0 )
			{
				nMin = nRed1;
				nMinIdx = h;
				if( nRed1 < nRed0 )
				{
					nDirection = -1;
					nRedBeforeDrop = nRed0;
				}
			} else if (nDirection < 0 )
			{
				// start traking the minimum
				if( nRed1 < nMin )
				{
					nMin = nRed1;
					nMinIdx = h;
				}
				if( nRed1 > nRed0)
				{
					// found a valley, started going up
					nDirection = 1;
					if( nMin < nRedBeforeDrop-nValley && nHIdx < nMaxValleys)
					{
						nValleyHList[nHIdx++] = nMinIdx;
					}
				}
			}
		}

		// now we have a list of valleys, output them to study
		if( nHIdx >= 6 )
		{
			if( bShowOutput )
			{
					cout << "w=" << w << " : ";
					for( int v = 0;v<nHIdx ; v++ )
						cout << (int) nValleyHList[v] << ",";
					cout << ", diffs=";
					for( int v = 0;v<nHIdx-1 ; v++ )
						cout << (int) (nValleyHList[v+1]-nValleyHList[v]) << ",";
					cout << "\r\n";
			}
			// find min spacing spacing
			int nMinDiff = 999;

			for( int v=0;v< nHIdx-1;v++ )
			{
				int nDiff = nValleyHList[v+1] - nValleyHList[v];
				if( nDiff < nMinDiff )
					nMinDiff = nDiff;
			}
			if(bShowOutput)
			    cout << "minDiff = " << nMinDiff << "\r\n";

			// find start sequence bars
			int nStart = 0;
			for( int i=0;i<nHIdx-1;i++)
			{
				int nDiff1 = abs(nValleyHList[i+1] - nValleyHList[i]);
				int nDiff2 = abs(nValleyHList[i+2] - nValleyHList[i+1]);
				if( nDiff1 < nMinDiff*2 && nDiff2 < nMinDiff*2 )
				{
					nStart = i+3;
					break;
				}
			}

			// find end sequence bars
			int nEnd = nHIdx-1-3;
			for( int i = nHIdx-1;i>nStart+2;i--)
			{
				int nDiff1 = abs(nValleyHList[i] - nValleyHList[i-1]);
				int nDiff2 = abs(nValleyHList[i-1] - nValleyHList[i-2]);
				if( nDiff1 < nMinDiff*2 && nDiff2 < nMinDiff*2 )
				{
					nEnd = i-3;
					break;
				}
			}

         if( bShowOutput )
			    cout << "nStart=" << nStart << ", nEnd="<<nEnd<< "\r\n";
			// parse area in between
			if( nEnd > nStart )
			{
				int cCode=0;
				int nCode[4];
				int nc=0;
				nCode[0]=0;
				nCode[1]=0;
				nCode[2]=0;
				nCode[3]=0;

				for( int i = nStart; i < nEnd;i++)
				{
					int nSteps = ((nValleyHList[i] - nValleyHList[i-1])/nMinDiff)-1;
					if( bShowOutput )
					    cout << "nSteps("<<i<<")="<<nSteps<< " , ";
					if( nSteps == 1 )
					{
					} else if( nSteps == 2 && nc<3)
					{
						nCode[nc+1]=1;
						nc++;
					} else if( nSteps == 3 && nc<2)
					{
						nCode[nc+2]=1;
						nc+=2;
					} else if( nSteps ==4 && nc<1)
					{
						nCode[nc+3]=1;
						nc+=3;
					}
				}

				if( nCode[3] > 0 )
					cCode |= 1;
				if( nCode[3] > 0 )
					cCode |= 2;
				if( nCode[3] > 0 )
					cCode |= 4;
				if( nCode[3] > 0 )
					cCode |= 8;
            if( bShowOutput )
				    cout << " Code=" << cCode;

				nCodesFound[w] = (int) cCode;
				nHeightFound[w] = (int) nValleyHList[nEnd+3] - nValleyHList[nStart-3];

			}
			if( bShowOutput)
			    cout << "\r\n";

		}

		if( 0 )
		for( int h=1;h<nHeight;h++)
		{
			// look for large magnitude transitions w-b-w-b-w-b-w - this needs to have a similar period. up to 4 bars will follow(the code), with 3 more bars to terminate
			char nRed0 = pImageData[w*3 + (h-1)*nWidthInBytes]; // use Red channel because green and blue are mostly noise
			char nRed1 = pImageData[w*3 + h*nWidthInBytes]; // use Red channel because green and blue are mostly noise
			if( nRed0 < nMinRed )
				nMinRed = nRed0;
			if( nRed0 > nMaxRed )
				nMaxRed = nRed0;
			if( nMode != 1 && nRed0 > nRed1+20)
			{
				// found white to black transition (or first transition)
				if( nFirstTransition ==0 )
					nFirstTransition = h;
				if( nPrevTransition > 0 )
				{
					if( nPeriod == 0)
						nPeriod = h-nPrevTransition; // first period
					else
						nPeriod = ((10*nPeriod + 10*(h-nPrevTransition))/2+5)/10; // get avg period with rounding

					if( nPrevPeriod > 0 && nPeriod>0 && 100*abs(nPeriod - nPrevPeriod)/nPeriod > 30 )
					{
						// bad period, over 30% different from previous, reset and start over
						nMode=0;
						nGoodPeriodCount=0;
						nPrevPeriod=0;
						nPrevTransition=0;
						nFirstTransition=0;
						nMinRed = 255;
						nMaxRed = 0;
					} else
					{
						// good period, continue looking
						if( bShowOutput)
								cout << "Found transition w->b cnt=" << nGoodPeriodCount << "\r\n";
						nGoodPeriodCount++;
						nPrevPeriod = nPeriod;
						nMode=1;
					}
				}
				nPrevTransition = h;
			} else if( nMode==1 && nRed0 < nRed1-20)
			{
				// found black to white transition
				nPeriod = h-nPrevTransition;
				if( nPrevPeriod > 0 && nPeriod>0 && 100*abs(nPeriod - nPrevPeriod)/nPeriod > 30 )
				{
					// bad period, over 30% different from previous, reset and start over
					nMode=0;
					nGoodPeriodCount=0;
					nPrevPeriod=0;
					nPrevTransition=0;
					nMinRed = 255;
					nMaxRed = 0;
				} else
				{
					// good period, continue looking
					if( bShowOutput )
							cout << "Found transition b->w cnt=" << nGoodPeriodCount << "\r\n";
					nGoodPeriodCount++;
					nPrevPeriod = nPeriod;
					nMode=2;
				}
				nPrevTransition = h;
			}

			if( nGoodPeriodCount==6)
			{
				if(bShowOutput)
					cout << "Found 6 good periods at w=" << w << ",top=" << nFirstTransition << "\r" << endl;
				// calc a better period
				nPeriod = (h-nFirstTransition)/6;
				// read the 4 bits, and test for the remaining 3
				char cCode = 0;
				int nBit1 = h + nPeriod*1;
				int nBit2 = h + nPeriod*3;
				int nBit3 = h + nPeriod*5;
				int nBit4 = h + nPeriod*7;

				char nMiddleIntensity = (nMinRed + nMaxRed)/2;

				if( h + nPeriod*15 >= nHeight )
					break; // found nothing for this column! we would go out of bounds looking at the h

				if(bShowOutput)
				{
					cout << "new Period=" << nPeriod << ", ";
				   cout << "Middle Intensity = " << (int)nMiddleIntensity << "\r\n";
					cout << "Info [";
				}
				int nSum = 0;
				for( int nb=nBit4; nb < nBit4 + nPeriod ; nb++ )
					nSum += pImageData[w*3 + nb*nWidthInBytes];
				nSum /= nPeriod;
				if( nSum > nMiddleIntensity )
					cCode |= 1;

				if(bShowOutput)
					cout << nSum << ",";

				nSum=0;
				for( int nb=nBit3; nb < nBit3 + nPeriod ; nb++ )
					nSum += pImageData[w*3 + nb*nWidthInBytes];
				nSum /= nPeriod;
				if( nSum > nMiddleIntensity )
					cCode |= 2;
				if(bShowOutput)
					cout << nSum << ",";

				nSum=0;
                                for( int nb=nBit2; nb < nBit2 + nPeriod ; nb++ )
                                        nSum += pImageData[w*3 + nb*nWidthInBytes];
				nSum /= nPeriod;
                                if( nSum > nMiddleIntensity )
                                        cCode |= 4;
				if(bShowOutput)
					cout << nSum << ",";

				nSum=0;
                                for( int nb=nBit4; nb < nBit4 + nPeriod ; nb++ )
                                        nSum += pImageData[w*3 + nb*nWidthInBytes];
				nSum /=nPeriod;
                                if( nSum > nMiddleIntensity )
                                        cCode |= 8;
				if(bShowOutput)
					cout << nSum << "]\r\n";


				// check for 1 more black and white strips to be a valid reading
				int bFoundTrailing = false;
				int nT1w = h + nPeriod*8;
				int nT1b = h + nPeriod*9;

                                int nSumW=0;
				int nSumB=0;
                                for( int nb=nT1w; nb < nT1w + nPeriod ; nb++ )
                                        nSumW += pImageData[w*3 + nb*nWidthInBytes];
				nSumW /= nPeriod;
                                if( nSumW > nMiddleIntensity )
				{
					nSumB=0;
		                        for( int nb=nT1b; nb < nT1b + nPeriod ; nb++ )
                	                        nSum += pImageData[w*3 + nb*nWidthInBytes];
					nSumB /= nPeriod;
					if( nSumB < nMiddleIntensity )
					{
						bFoundTrailing = true;
					}
				}
				if(bShowOutput)
						cout << "Code="<<(int)cCode<<"\r"<<endl;
				if( !bFoundTrailing )
				{
					if(bShowOutput)
					{
						cout << "Did not find end bar, Discarded the code -> ";
						cout << "[" << nSumW << "," << nSumB << "] \r" << endl;
					}
				} else
				{
					// save code
					int nCode = (int) cCode;
					if(bShowOutput)
						cout << "Code Saved = " << nCode << " at w="<<w<<" first="<<nFirstTransition << "\r" << endl;
					nCodesFound[w] = (int) cCode;
					nHeightFound[w] = (int) nPeriod*10; // not done, would like to read h at end of barcode
				}

				break; // done this column!
			}
		}
	}


	// find center for each barcode
	for( int w = 0 ; w<nWidth ; w++)
	{
		if( nCodesFound[w] > 0 )
		{
			int nStartIdx = w;
			int nEndIdx = w;
			int nSrchCode = nCodesFound[w];
			int nSrchHeight = nHeightFound[w];
			for( int w2 = w ; w2<nWidth ; w2++)
			{
				if( nCodesFound[w2] == nCodesFound[nStartIdx] )
				{
					nEndIdx = w2;
				}
			}
			// clear range
			for( int w2 = w ; w2<nWidth ; w2++)
			{
				nCodesFound[w2] = 0;
				nHeightFound[w2] = 0;
			}
			// save the code in the center of the found codes for best angle approximation
			nCodesFound[(nEndIdx+nStartIdx)/2] = nSrchCode;
			nHeightFound[(nEndIdx+nStartIdx)/2] = nSrchHeight;
		}
	}

	// now count the results, and save to the member vars
	if(bShowOutput)
		cout << "Final Barcode List:\r\n";
	double deg_per_pixel = 1.0;
	for( int w = 0 ; w<nWidth ; w++)
	{
		if( nCodesFound[w] > 0 )
		{
			this->nBarValue[this->nNumBarcodesFound] = nCodesFound[w]; // the Barcode Value identified (1-...)
			this->aAngle[this->nNumBarcodesFound] = (w - nWidth/2)*deg_per_pixel; // this array contains angles to each barcode in degrees
			this->aDepth[this->nNumBarcodesFound] = nHeightFound[w]*deg_per_pixel; // contains estimate of distance to the barcode (meters) based on the height in pixels of the total
			this->nNumBarcodesFound++;
			if(bShowOutput)
				cout << "Barcode " << nCodesFound[w] << " at " << w << " with Depth=" << aDepth[this->nNumBarcodesFound] << "\r" << std::endl;
		}
	}

        unsigned long nEnd_ms = this->getTime_ms();

	if(bShowOutput)
		cout << "ProcessImage completed in " << (nEnd_ms-nStart_ms) << " ms\r" << std::endl;
	return this->nNumBarcodesFound;
}
