# README #

This C++ application depends on the wiringPi library.

### Requires
wiringPi Library from wiringpi.com

### What is this repository for? ###

* Control a Raspberry Pi computer to run a robot lawn mower.
* Version 0.0.1
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up

1. Install wiringPi according to the instructions on http://www.WiringPi.com

2. Install raspicam according to instructions on http://sourceforge.net/projects/raspicam/?source=directory

3. Install Raspberry Pi Streaming solution 'UV4L' on http://www.linux-projects.org/modules/sections/index.php?op=viewarticle&artid=14

4. git clone this repo

5. cd raspimower

6. run ./build.sh or compile with the codeblocks project file that is included.

7. Copy the contents of the www subdirectory into /var/www to run as localhost (you will need to install apache and php and memcached)

### Who do I talk to? ###

* Repo owner or admin