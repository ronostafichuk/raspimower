<head>
<meta name="viewport" content="width=device-width, initial-scale=1,user-scalable=no">
</head>
<?php
error_reporting(true);
ini_set('display_errors', 'On');

$m = new Memcached();
$m->addServer('localhost',11211,1);

// check if this is the first time running the web page 
if( !is_numeric($m->get("status"))  || $m->get("status") == false )
{
	// set up cache for the first time
	$m->set("status",0);
	$m->set("mode","Waiting for Orders");  		
	$m->set("manual_override",0);
	$m->set("headLightsStatus","Off");
	$m->set("runningLightsStatus","Off");
	$m->set("mowerStatus","Off");
	$m->set("motion_direction","Stopped");
	$m->set("joystickX",0);
	$m->set("joystickY",0);
	$m->set("joystick_ms",0);
}

?>
<div id="divVideo" style="width:340px;height:240px">
    <video id="remote-video" autoplay=""  style="border:1px solid" width="400" height="310">
        Your browser does not support the video tag.
    </video>
</div>

<div id="divJoystick">
<table>
<tr>
	<td>
		<canvas id="canvasJoystick" width="250" height="250" style="width:250px;height:250px;background-color:orange">
		</canvas>
	</td>
	<td><canvas id='canvasLeftMotor' width='80px' height='250px' ></canvas></td>
	<td><canvas id='canvasRightMotor' width='80px' height='250px' ></canvas></td>
</tr>
</table>
<div id="divDebug">Debug</div>
</div>

<div id="controls" style="width:533px">
   Video Server Address: <input required type="text" id="signalling_server" value="127.0.0.1:8080" title="<host>:<port>, default address is autodetected"/> 
   <button style="height:40px" id="start" onclick="start();">Start</button>
   <button style="height:40px" disabled id="stop" onclick="stop();">Stop</button><br>
	
<table style="border:2px solid black" >
<tr><td style="text-align:right"><button style="height:40px"  onclick="sendCommand('Pivot Left');">Pivote Left</button></td>
<td></td><td><button style="height:40px"  onclick="sendCommand('Pivot Right');">Pivot Right</button></td>
<td rowspan="4" >
<b>Audio:</b><br>
<button style="height:40px"  onclick="sendCommand('Play Exterminate');">Exterminate!</button><br>
<button style="height:40px"  onclick="sendCommand('Play Destroy');">Destroy</button><br>
<button style="height:40px" onclick="sendCommand('Play Random');">Random Audio Clip</button><br>
</td>
<td rowspan="4" >
	<button style="height:40px" onclick="sendCommand('Start Patrol');">Patrol</button><br>
 	<button style="height:40px" onclick="sendCommand('Start Mowing');">Cut Grass</button><br>
 	<button style="height:40px" onclick="sendCommand('Start Locate');">Locate</button> <br>
 	<button style="height:40px" onclick="sendCommand('Start Park');">Park</button> <br>
 	<button style="height:40px" onclick="sendCommand('Shoot');">Fire Nerfgun</button><br>
</td>
</tr>
<tr><td></td><td style="text-align:center"><button style="height:40px" onclick="sendCommand('Move Forward');">Forward</button></td><td></td></tr>
<tr>
	<td style="text-align:right"><button style="height:40px" onclick="sendCommand('Turn Left');">Left</button></td>
	<td style="text-align:center"><button style="height:40px" onclick="sendCommand('Stop');">Stop</button></td>
	<td><button style="height:40px" onclick="sendCommand('Turn Right');">Right</button></td>
</tr>
<tr><td></td><td><button style="height:40px" onclick="sendCommand('Move Backward');">Backward</button></td><td></td></tr>
</table>

	Lights Head: <button style="height:40px" onclick="sendCommand('Head Lights On');">On</button><button style="height:40px" onclick="sendCommand('Head Lights Off');">Off</button>&nbsp;
	Body: <button style="height:40px" onclick="sendCommand('Running Lights On');">On</button><button style="height:40px" onclick="sendCommand('Running Lights Off');">Off</button>&nbsp;
	Mower: <button style="height:40px" onclick="sendCommand('Mower On');">On</button><button style="height:40px" onclick="sendCommand('Mower Off');">Off</button>
	<br>
	Speech: <input id="iText" size=30 type="Text" /><button style="height:40px" onclick="sendCommand('Speak');">Speak</button><br>

	<div id="divMC"></div><br>
</div>
<script src="jquery-2.1.4.min.js" ></script>
<script type="text/javascript" src="ROFuel.js"></script>
<script>
	var _intervalMC;
	var _intervalJoystick; // timer used to send regular joystick events
	var _joystickTouched = false, joyX=0, joyY=0, joyStartX=0,joyStartY=0;
	var _joy = $("#canvasJoystick");
	var _ctx = document.getElementById('canvasJoystick').getContext("2d");
		
	$(document).ready(function(){		
			// update the memcached values every 1/2 second
			getMemCachedValues();
			_intervalMC = setInterval(function() {getMemCachedValues(); },500);
			_intervalJoystick = setInterval(function() {sendJoystickReading(); },100);
						
			// start(); // do not start video stream by default
   	  	
   	  	// prepare joystick for use using touch interface											 			
			_joy.bind('touchmove', function(event){ 
				joyMove(event);
			});			
			_joy.bind('touchstart', function(event){
				joyStart(event);
			});							
		  _joy.bind('touchend', function(event){
				joyEnd(event);
			});
			
			
// bind mouse events
			_joy.bind('mousemove', function(event){ 
				joyMove(event);
			});			
			_joy.bind('mousedown', function(event){
				joyStart(event);
			});							
		  _joy.bind('mouseup', function(event){
				joyEnd(event);
			});
			
			
		});
		
function joyStart(event)
{
		event.preventDefault();
		_joystickTouched = true;
		joyX = 0;
		joyY = 0;
		
		if( event.originalEvent.touches !== undefined )
		{
			joyStartX =event.originalEvent.touches[0].pageX-_joy.offset().left;
			joyStartY =event.originalEvent.touches[0].pageY-_joy.offset().top;
		} else if( event.pageX !== undefined )
		{
			joyStartX = event.pageX-_joy.offset().left;
			joyStartY = event.pageY-_joy.offset().top;
		} else
		{
			$('#divDebug').html("No input found");
		}
		_ctx.fillStyle = 'yellow';		
		_ctx.fillRect(0,0,500,500);
		
		_ctx.beginPath();
		_ctx.fillStyle = '#0000FF';
		_ctx.fillRect(joyStartX-5,0,10,250);
		_ctx.fillRect(0,joyStartY-5,250,10);

		_ctx.fillStyle = '#FF0000';
		_ctx.fillRect(joyX+joyStartX-5,joyY+joyStartY-5,10,10);		
}
function joyMove(event)
{
	   event.preventDefault();
		if( !_joystickTouched)
			return;
		if( event.originalEvent.touches !== undefined )
		{
			joyX = Math.floor(event.originalEvent.touches[0].pageX -_joy.offset().left - joyStartX);
			joyY = Math.floor(event.originalEvent.touches[0].pageY -_joy.offset().top - joyStartY);
			
			$('#divDebug').html("coord " + joyX + "," + joyY + " d " + event.originalEvent.touches[0].pageX + "," + event.originalEvent.touches[0].pageY );
		} else if( event.pageX !== undefined )
		{
			joyX = Math.floor(event.pageX -_joy.offset().left - joyStartX);
			joyY = Math.floor(event.pageY -_joy.offset().top - joyStartY);
			$('#divDebug').html("coord " + joyX + "," + joyY  );
		} else
		{
			$('#divDebug').html("No Input");
		}

		_ctx.fillStyle = 'yellow';		
		_ctx.fillRect(0,0,500,500);
		
		_ctx.beginPath();
		_ctx.fillStyle = '#0000FF';
		_ctx.fillRect(joyStartX-5,0,10,250);
		_ctx.fillRect(0,joyStartY-5,250,10);
		
		_ctx.fillStyle = '#FF0000';
		_ctx.fillRect(joyX+joyStartX-5,joyY+joyStartY-5,10,10);
}

function joyEnd(event)
{
	event.preventDefault();
   joyX=0;
   joyY=0;
   sendJoystickReading();
   _joystickTouched = false; // stop sending after this event
   
	_ctx.fillStyle = 'orange';		
	_ctx.fillRect(0,0,500,500);
	
	$('#divDebug').html("");
}	
	
	function getMemCachedValues()
	{
		// get values once every 500 ms 
		$.ajax({
  			url: "ajax.php",
  			data: { getMC: 1, },
  			context: document.body
		}).done(function(data) {
  			$("#divMC").html(data);
  			
  			// update motor speeds from the memcache data
  			var nLeft = data.indexOf("LeftMotorPWM=")+13;  			
  			var nEnd = data.indexOf(",",nLeft);  			
  			var nLeftPWM = 1*data.substring(nLeft,nEnd);
  			
			var nRight = data.indexOf("RightMotorPWM=")+14;
			var nEnd = data.indexOf(",",nRight);
			var nRightPWM = 1*data.substring(nRight,nEnd);
			
			var sParamLeft = "title=Left;min=-100;ll=-90;l=-50;h=50;hh=90;max=100;val="+nLeftPWM+";display="+nLeftPWM+" %";
			var sParamRight = "title=Right;min=-100;ll=-90;l=-50;h=50;hh=90;max=100;val="+nRightPWM+";display="+nRightPWM+" %";
			try
			{
				drawFuelGauge('canvasLeftMotor',sParamLeft);
				drawFuelGauge('canvasRightMotor',sParamRight);
			} catch(err)
			{
			}
		});
	}

	function sendJoystickReading()
	{
			// sends once every 100ms
		   if( !_joystickTouched )
				return;
			
			$.ajax({
  					url: "ajax.php",
  					method: "POST",
  					data: { "joystickX":joyX, "joystickY":joyY  },
  					context: document.body
				}).done(function() {
  					// do nothing
				});		
	}
	
	function sendCommand(sCmd)
	{
			if( sCmd == "Speak" )
			{
				var sText = $("#iText").val();
				$.ajax({
  					url: "ajax.php",
  					method: "POST",
  					data: { userCommand: sCmd, userText: sText},
  					context: document.body
				}).done(function() {
  					getMemCachedValues();
				});
			} else
			{
				$.ajax({
  					url: "ajax.php",
  					method: "POST",
  					data: { userCommand: sCmd, },
  					context: document.body
				}).done(function() {
  					getMemCachedValues();
				});
			}		
	}	



            var signalling_server_hostname = location.hostname || "127.0.0.1";
            var signalling_server_address = signalling_server_hostname + ':' + (location.port || 80);
				signalling_server_address = signalling_server_hostname + ':8080';

            addEventListener("DOMContentLoaded", function() {
                document.getElementById('signalling_server').value = signalling_server_address;
            });

            var ws = null;
            var pc;
            var pcConfig = {"iceServers": [
                    {urls: ["stun:stun.l.google.com:19302", "stun:" + signalling_server_hostname + ":3478"]}
                ]};
            var pcOptions = {
                optional: [
                    {DtlsSrtpKeyAgreement: true}
                ]
            };
            var mediaConstraints = {
                optional: [],
                mandatory: {
                    OfferToReceiveAudio: true,
                    OfferToReceiveVideo: true
                }
            };

            RTCPeerConnection = window.mozRTCPeerConnection || window.webkitRTCPeerConnection;
            RTCSessionDescription = window.mozRTCSessionDescription || window.RTCSessionDescription;
            RTCIceCandidate = window.mozRTCIceCandidate || window.RTCIceCandidate;
            getUserMedia = navigator.mozGetUserMedia || navigator.webkitGetUserMedia;
            URL = window.webkitURL || window.URL;

            function createPeerConnection() {
                try {
                    pc = new RTCPeerConnection(pcConfig, pcOptions);
                    pc.onicecandidate = onIceCandidate;
                    pc.onaddstream = onRemoteStreamAdded;
                    pc.onremovestream = onRemoteStreamRemoved;
                    console.log("peer connection successfully created!");
                } catch (e) {
                    console.log("createPeerConnection() failed");
                }
            }

            function onIceCandidate(event) {
                if (event.candidate) {
                    var candidate = {
                        sdpMLineIndex: event.candidate.sdpMLineIndex,
                        sdpMid: event.candidate.sdpMid,
                        candidate: event.candidate.candidate
                    };
                    var command = {
                        command_id: "addicecandidate",
                        data: JSON.stringify(candidate)
                    };
                    ws.send(JSON.stringify(command));
                } else {
                    console.log("End of candidates.");
                }
            }

            function onRemoteStreamAdded(event) {
                console.log("Remote stream added:", URL.createObjectURL(event.stream));
                var remoteVideoElement = document.getElementById('remote-video');
                remoteVideoElement.src = URL.createObjectURL(event.stream);
                remoteVideoElement.play();
            }

            function onRemoteStreamRemoved(event) {
                var remoteVideoElement = document.getElementById('remote-video');
                remoteVideoElement.src = '';
            }

            function start() {
                if ("WebSocket" in window) {
                    document.getElementById("stop").disabled = false;
                    document.getElementById("start").disabled = true;
                    document.documentElement.style.cursor ='wait';
                    server = document.getElementById("signalling_server").value.toLowerCase();

                    ws = new WebSocket('ws://' + server + '/stream/webrtc');
                    ws.onopen = function () {
                        console.log("onopen()");
                        createPeerConnection();
                        var command = {
                            command_id: "offer"
                        };
                        ws.send(JSON.stringify(command));
                        console.log("onopen(), command=" + JSON.stringify(command));
                    };

                    ws.onmessage = function (evt) {
                        var msg = JSON.parse(evt.data);
                        //console.log("message=" + msg);
                        console.log("type=" + msg.type);

                        switch (msg.type) {
                            case "offer":
                                pc.setRemoteDescription(new RTCSessionDescription(msg),
                                    function onRemoteSdpSuccess() {
                                        console.log('onRemoteSdpSucces()');
                                        pc.createAnswer(function (sessionDescription) {
                                            pc.setLocalDescription(sessionDescription);
                                            var command = {
                                                command_id: "answer",
                                                data: JSON.stringify(sessionDescription)
                                            };
                                            ws.send(JSON.stringify(command));
                                            console.log(command);

                                        }, function (error) {
                                            alert("Failed to createAnswer: " + error);

                                        }, mediaConstraints);
                                    },
                                    function onRemoteSdpError(event) {
                                        alert('Failed to setRemoteDescription: ' + event);
                                    }
                                );

                                var command = {
                                    command_id: "geticecandidate"
                                };
                                console.log(command);
                                ws.send(JSON.stringify(command));
                                break;

                            case "answer":
                                break;

                            case "message":
                                alert(msg.data);
                                break;

                            case "geticecandidate":
                                var candidates = JSON.parse(msg.data);
                                for (var i = 0; i < candidates.length; i++) {
                                    var elt = candidates[i];
                                    var candidate = new RTCIceCandidate({sdpMLineIndex: elt.sdpMLineIndex, candidate: elt.candidate});
                                    pc.addIceCandidate(candidate,
                                        function () {
                                            console.log("IceCandidate added: " + JSON.stringify(candidate));
                                        },
                                        function (error) {
                                            console.log("addIceCandidate error: " + error);
                                        }
                                    );
                                }
                                document.documentElement.style.cursor ='default';
                                break;
                        }
                    };

                    ws.onclose = function (evt) {
                        if (pc) {
                            pc.close();
                            pc = null;
                        }
                        document.getElementById("stop").disabled = true;
                        document.getElementById("start").disabled = false;
                        document.documentElement.style.cursor ='default';
                    };

                    ws.onerror = function (evt) {
			var msg = JSON.parse(evt.data);
                        alert("An error has occurred!" + msg);
                        ws.close();
                    };

                } else {
                    alert("Sorry, this browser does not support WebSockets.");
                }
            }

            function stop() {
                if (pc) {
                    pc.close();
                    pc = null;
                }
                if (ws) {
                    ws.close();
                    ws = null;
                }
                document.getElementById("stop").disabled = true;
                document.getElementById("start").disabled = false;
                document.documentElement.style.cursor ='default';
            }

            function mute() {
                var remoteVideo = document.getElementById("remote-video");
                remoteVideo.muted = !remoteVideo.muted;
            }

            function fullscreen() {
                var remoteVideo = document.getElementById("remote-video");
                if(remoteVideo.requestFullScreen){
                    remoteVideo.requestFullScreen();
                } else if(remoteVideo.webkitRequestFullScreen){
                    remoteVideo.webkitRequestFullScreen();
                } else if(remoteVideo.mozRequestFullScreen){
                    remoteVideo.mozRequestFullScreen();
        	}
            }

            window.onbeforeunload = function() {
                if (ws) {
                    ws.onclose = function () {}; // disable onclose handler first
                    stop();
                }
            };

        </script>