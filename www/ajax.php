<?php
error_reporting(true);
ini_set('display_errors', 'On');

$m = new Memcached();
$m->addServer('localhost',11211,1);

if( isset($_REQUEST['joystickX'] ))
{
		// save the joystick reading with the current timestamp in ms
		$m->set('joystick_ms',microtime(true)*1000);
		$m->set('joystickX',$_REQUEST['joystickX']);
		$m->set('joystickY',$_REQUEST['joystickY']);
}
 	
if( isset($_REQUEST['getMC'] ))
{
	// first check for stale command and clear it (Would use memcached, but it does not seem to be expiring items properly)
	if( $m->get('userCommand_ms') < microtime(true)*1000 - 4000 )
	{
		// stale, clear old command
		$m->set('userCommand_ms',0);
		$m->set('userCommand','');
	}
	
	// get html formatted values from Memcached
	echo '<span>Status='.$m->get("status").'</span>&nbsp;
 Mode='.$m->get("mode").'</span>&nbsp;
Head Lights='.$m->get("headLightsStatus").';&nbsp;
Running Lights='.$m->get("runningLightsStatus").'&nbsp;
Mower='.$m->get("mowerStatus").' Motion='.$m->get("motionStatus").'<br>
Last User Command= '.$m->get('prevUserCommand').'<br>
Joystick ('.$m->get("joystickX").' , '.$m->get("joystickY").')<br>';

echo 'LeftMotorPWM=';
if($m->get("LeftMotorPWM") ) {
	echo $m->get("LeftMotorPWM");
} else { 
	echo '0';
}

echo ',RightMotorPWM=';
if( $m->get("RightMotorPWM") ){
	echo $m->get("RightMotorPWM");
} else {
	echo '0';
}
echo ','; // need this for string parse by browser

	return;
}

if( isset($_REQUEST['userCommand']))
{
		$m->set("userCommand", $_REQUEST['userCommand']);
		$m->set("userCommand_ms", floor(microtime(true)*1000));
}

if( isset($_REQUEST['userText']))
{
		$m->set("userText", $_REQUEST['userText']);
}
