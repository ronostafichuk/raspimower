// Copyright (C) 2012 Ron Ostafichuk
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files
// (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify,
// merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
// IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


// Function to draw an html5 fuel type Gauge using the canvas tag
/*
 * drawFuelGauge params:
 * sControlID = DOM id for canvas element (canvas should have dimensions 2*higher than it is tall for best results)
 * sParams = semicolon delimited string containing
 *  title= text string for top right of gauge
 *  min= integer, lowest value to show on gauge
 *  ll= integer, start of the low emergency level
 *  l= integer, start of the low caution level
 *  h= integer, start of the high caution
 *  hh= integer, start of the high emergency level
 *  max= integer, highest value to show on gauge
 *  val= integer, needle point level
 *  display= friendly text for display at bottom of gauge
 */
function drawFuelGauge(sControlID, sParams) {
	var canvas = document.getElementById(sControlID);
	if( canvas == 'undefined' )
		return; // canvas does not exist
	if (!canvas.getContext) {
		$('#' + sControlID).html('Unsupported: ' + sParams);
		return; // this browser does not support the canvas (html5)
	}
	var ctx = canvas.getContext('2d');
	
	// default inputs for debug, similar to json format
	if (sParams.length <= 0)
		sParams = "arrow=1;title=Sample;min=0;ll=5;l=10;h=30;hh=45;max=100;val=28;display=28 %;background-color=rgba(255,255,255,0)"; 
	var aLabels = []; // used to put labels on image with x,y,l and optional text color c
	var NA = -9e9; // not available
	var nMin = NA, nMax = NA, nL = NA, nLL = NA, nH = NA, nHH = NA, nReading = NA;
	var sDisplay = ""; // text to be displayed for the gauge reading
	var sTitle = ""; // title for the gauge
	var nFuelLevel_px = 1;
	var aParams = sParams.split(";"); // param format key=val;

	// basic colors need for drawing
	var sBlack = '#000000';
	var sGray1 = '#E1E1E1', sGray2 = '#A4A4A4';
	var sBlue = '#0000FF';

	var sRed = '#EE0000', sDarkRed = '#900000';
	var sDarkYellow='#DaDa00',sYellow = '#FaFa00';

	var sBackgroundColor = ''; // transparent background
	
	// get all needed params from the input string
	for ( var i = 0; i < aParams.length; i++) {
		var aKeyVal = aParams[i].split("=");
		var sVal = aKeyVal[1];
		var nVal = parseFloat(sVal);
		switch( aKeyVal[0] )
		{
			case "arrow":
				if( nVal != NaN )
					nFuelLevel_px = nVal;
				break;
			case "title":
				sTitle = sVal;
				break;
			case "min":
				if( nVal != NaN )
					nMin = nVal;
				break;
			case "max":
				if( nVal != NaN )
					nMax = nVal;
				break;
			case "l":
				if( nVal != NaN )
					nL = nVal;
				break;
			case "ll":
				if( nVal != NaN )
					nLL = nVal;
				break;
			case "h":
				if( nVal != NaN )
					nH = nVal;
				break;
			case "hh":
				if( nVal != NaN )
					nHH = nVal;
				break;
			case "val":
				nReading = nVal;
				break;
			case "display":
				sDisplay = sVal;
				break;
			case "background-color":
				sBackgroundColor = sVal;
				break;
			default:
		}			
	}
	// prevent bad values
	if (nL <= nMin)
		nL = NA;
	if (nLL <= nMin)
		nLL = NA;
	if (nH <= nMin)
		nH = NA;
	if (nHH <= nMin)
		nHH = NA;

	// get some critical dimensions for drawing
	var nWidth = ctx.canvas.width;
	var nHeight = ctx.canvas.height;

	// figure out the font size to use
	var nFontHeight = Math.floor(nHeight * 14 / 240); // estimate based on 14px
	if (nFontHeight < 7)
		nFontHeight = 7;
	if (nFontHeight > 28)
		nFontHeight = 28;
	var sFont = nFontHeight + "px 'arial'";

	ctx.font = sFont;

	// clear the gauge area
	ctx.fillstyle = sBackgroundColor;
	ctx.lineCap = "square"; // most browsers only support square linecaps
	if( sBackgroundColor.length > 0 )
		ctx.fillRect(0, 0, nWidth, nHeight); // clear the gauge area according to user requested rgba
	else
		ctx.clearRect(0,0,nWidth,nHeight);

	// calc top and bottom of gauge
	nBottom_px=nHeight*0.8;
	nTop_px=nHeight*0.11;
	nPixelRange = nTop_px-nBottom_px;
	nLeft_px = nWidth*0.25;
	nRight_px = nWidth*0.50;
	if (nReading < nMin) 
	{
		nReading = nMin; // do not allow arrow to go below the minimum gauge position!
	}
	if (nReading > nMax) 
	{
		for ( var nOrder = 0.00000001; nOrder < 1000000000; nOrder *= 10) 
		{
			if (nReading > nOrder)
				nMax = nOrder * (Math.floor(nReading / nOrder) + 1); 
			// prevent reading from being greater than the max
		}
	}
	var nRange = nMax-nMin;
	
	// default params as not avalable
	var nRedMin = NA, nYellowMin = NA, nYellowMax = NA, nRedMax = NA;
	// get the pixel position for each level
	var nPixelsPerDegree = nPixelRange / nRange;
	if (nLL > NA)
		nRedMin = (nLL-nMin) * nPixelsPerDegree + nBottom_px;
	if (nL > NA)
		nYellowMin = (nL-nMin) * nPixelsPerDegree + nBottom_px;
	if (nH > NA)
		nYellowMax = (nH-nMin) * nPixelsPerDegree + nBottom_px;
	if (nHH > NA)
		nRedMax = (nHH-nMin) * nPixelsPerDegree + nBottom_px;

	nFuelLevel_px = (nReading-nMin) * nPixelsPerDegree + nBottom_px;

	// Set gauge face color
	var sTextColorForReading = sBlack;
	var sColorOfLiquid = sBlue;
	
	// adjust liquid color if in alarm ranges
	if( nRedMin != NA && nFuelLevel_px >= nRedMin )
		sColorOfLiquid = sRed;
	else if( nYellowMin != NA && nFuelLevel_px >= nYellowMin )
		sColorOfLiquid = sYellow;
	else if( nRedMax != NA && nFuelLevel_px <= nRedMax )
		sColorOfLiquid = sRed;
	else if( nYellowMax != NA && nFuelLevel_px <= nYellowMax )
		sColorOfLiquid = sYellow;

	// draw body of gauge	
	ctx.strokeStyle = '#000000';
	ctx.save();
	ctx.lineWidth = 1;
	ctx.beginPath();
	ctx.moveTo(nLeft_px, nBottom_px);
	ctx.lineTo(nLeft_px, nTop_px);
	ctx.lineTo(nRight_px, nTop_px);
	ctx.lineTo(nRight_px, nBottom_px);
	ctx.closePath();
	ctx.stroke();
	
	ctx.restore();
	
	// draw liquid at the level requested
	ctx.fillStyle = sColorOfLiquid;
	ctx.save();
	ctx.beginPath();
	ctx.moveTo(nLeft_px+1, nBottom_px-1);
	ctx.lineTo(nLeft_px+1, nFuelLevel_px);
	ctx.lineTo(nRight_px-1, nFuelLevel_px);
	ctx.lineTo(nRight_px-1, nBottom_px-1);
	ctx.closePath();
	ctx.fill();
	ctx.restore();
	
	var nRangeWidth_px = (nRight_px-nLeft_px)*0.2;
	var nRangeLeft_px = nLeft_px + 1;
	if (nYellowMin != NA) {
		// check for red min defined
		if( nRedMin == NA )
			nRedMin = nBottom_px;
		// yellow min part
		ctx.fillStyle = sDarkYellow;
		ctx.save();
		ctx.beginPath();
		ctx.moveTo(nRangeLeft_px, nRedMin);
		ctx.lineTo(nRangeLeft_px, nYellowMin);
		ctx.lineTo(nRangeLeft_px+nRangeWidth_px, nYellowMin);
		ctx.lineTo(nRangeLeft_px+nRangeWidth_px, nRedMin);
		ctx.closePath();
		ctx.fill();
		ctx.stroke();
		ctx.restore();

		// highlight
		ctx.fillStyle = sYellow;
		ctx.save();
		ctx.beginPath();
		ctx.moveTo(nRangeLeft_px+nRangeWidth_px*0.3, nRedMin-1);
		ctx.lineTo(nRangeLeft_px+nRangeWidth_px*0.3, nYellowMin+1);
		ctx.lineTo(nRangeLeft_px+nRangeWidth_px*0.7, nYellowMin+1);
		ctx.lineTo(nRangeLeft_px+nRangeWidth_px*0.7, nRedMin-1);
		ctx.closePath();
		ctx.fill();
		ctx.restore();

	}
	if (nMin < nLL && nLL < nMax && nRedMin != nBottom_px ) {
		// red min part (do not draw if at very bottom)
		ctx.fillStyle = sDarkRed;
		ctx.save();
		ctx.beginPath();
		ctx.moveTo(nRangeLeft_px, nBottom_px);
		ctx.lineTo(nRangeLeft_px, nRedMin);
		ctx.lineTo(nRangeLeft_px+nRangeWidth_px, nRedMin);
		ctx.lineTo(nRangeLeft_px+nRangeWidth_px, nBottom_px);
		ctx.closePath();
		ctx.fill();
		ctx.stroke();
		ctx.restore();

		ctx.fillStyle = sRed;
		ctx.save();
		ctx.beginPath();
		ctx.moveTo(nRangeLeft_px+nRangeWidth_px*0.3, nBottom_px-1);
		ctx.lineTo(nRangeLeft_px+nRangeWidth_px*0.3, nRedMin+1);
		ctx.lineTo(nRangeLeft_px+nRangeWidth_px*0.7, nRedMin+1);
		ctx.lineTo(nRangeLeft_px+nRangeWidth_px*0.7, nBottom_px-1);
		ctx.closePath();
		ctx.fill();
		ctx.restore();
	}
	// yellow max part
	if (nMin < nH && nH < nMax) {
		// check that red max exists
		if( nRedMax == NA )
			nRedMax = nTop_px;
		
		ctx.fillStyle = sDarkYellow;
		ctx.save();
		ctx.beginPath();
		ctx.moveTo(nRangeLeft_px, nYellowMax);
		ctx.lineTo(nRangeLeft_px, nRedMax);
		ctx.lineTo(nRangeLeft_px+nRangeWidth_px, nRedMax);
		ctx.lineTo(nRangeLeft_px+nRangeWidth_px, nYellowMax);
		ctx.closePath();
		ctx.fill();
		ctx.stroke();
		ctx.restore();

		// highlight
		ctx.fillStyle = sYellow;
		ctx.save();
		ctx.beginPath();
		ctx.moveTo(nRangeLeft_px+nRangeWidth_px*0.3, nYellowMax-1);
		ctx.lineTo(nRangeLeft_px+nRangeWidth_px*0.3, nRedMax+1);
		ctx.lineTo(nRangeLeft_px+nRangeWidth_px*0.7, nRedMax+1);
		ctx.lineTo(nRangeLeft_px+nRangeWidth_px*0.7, nYellowMax-1);
		ctx.closePath();
		ctx.fill();
		ctx.restore();
	}
	// red max part
	if (nMin < nHH && nHH < nMax && nRedMax != nTop_px) {
		ctx.fillStyle = sDarkRed;
		ctx.save();
		ctx.beginPath();
		ctx.moveTo(nRangeLeft_px, nRedMax);
		ctx.lineTo(nRangeLeft_px, nTop_px);
		ctx.lineTo(nRangeLeft_px+nRangeWidth_px, nTop_px);
		ctx.lineTo(nRangeLeft_px+nRangeWidth_px, nRedMax);
		ctx.closePath();
		ctx.fill();
		ctx.stroke();
		ctx.restore();

		// highlight
		ctx.fillStyle = sRed;
		ctx.save();
		ctx.beginPath();
		ctx.moveTo(nRangeLeft_px+nRangeWidth_px*0.3, nRedMax-1);
		ctx.lineTo(nRangeLeft_px+nRangeWidth_px*0.3, nTop_px+1);
		ctx.lineTo(nRangeLeft_px+nRangeWidth_px*0.7, nTop_px+1);
		ctx.lineTo(nRangeLeft_px+nRangeWidth_px*0.7, nRedMax-1);
		ctx.closePath();
		ctx.fill();
		ctx.restore();		
	}

	// draw ticks and labels for the gauge readings
	// calculate the number of major and minor ticks (6 major ticks, 4
	// divisions)
	var nBestMajorDiv = Math.ceil(Math.abs(nRange) / 10);
	var numMajorDivs = Math.ceil(nRange / nBestMajorDiv);
	// limit to a reasonable range
	while (numMajorDivs > 10.0)
		numMajorDivs /= 2;
	while (numMajorDivs < 4.0)
		numMajorDivs *= 2;

	var numMinorDivsPerMajor = 4.0; // default

	// tick logic
	var nTickThickness = Math.ceil(nWidth*0.002);
	var nTickWidth = Math.ceil(nWidth*0.05);
	
	var aSmallTicks = new Array();
	var aLargeTicks = new Array();

	majorTickStep_px = nPixelRange / numMajorDivs;
	minorTickStep_px = majorTickStep_px / numMinorDivsPerMajor;
	
	// try to calc best ticks to show
	if( nMin <= 0 && nMax >= 0 )
	{
		// have a zero on the plot, so build in both directions from zero
		var nZero_px = (0-nMin) * nPixelsPerDegree + nBottom_px;

		var iLoop = 0;
		var nSmallTick_Idx = 0;
		for ( var nStep_px = nZero_px; nStep_px >= nTop_px && nStep_px <= nBottom_px; nStep_px += minorTickStep_px) {
			if (iLoop++ % numMinorDivsPerMajor == 0)
				continue; // skip this tick
			aSmallTicks[nSmallTick_Idx++] = nStep_px;
		}
		var nLargeTick_Idx=0;
		for ( var nStep_px = nZero_px; nStep_px >= nTop_px-1 && nStep_px <= nBottom_px; nStep_px += majorTickStep_px) {
			aLargeTicks[nLargeTick_Idx++] = nStep_px;
		}

		var iLoop = 0;
		for ( var nStep_px = nZero_px; nStep_px >= nTop_px && nStep_px <= nBottom_px ; nStep_px -= minorTickStep_px) {
			if (iLoop++ % numMinorDivsPerMajor == 0)
				continue; // skip this tick
			aSmallTicks[nSmallTick_Idx++] = nStep_px;
		}

		for ( var nStep_px = nZero_px; nStep_px >= nTop_px && nStep_px <= nBottom_px; nStep_px -= majorTickStep_px) {
			aLargeTicks[nLargeTick_Idx++] = nStep_px;
		}		
	} else
	{
		// range does not go through zero so do it as simply as possible
		// draw small ticks only
		var iLoop = 0;
		var nSmallTick_Idx = 0;
		for ( var nStep_px = 0; nStep_px < -nPixelRange; nStep_px += minorTickStep_px) {
			if (iLoop++ % numMinorDivsPerMajor == 0)
				continue; // skip this tick
			// draw small ticks first as small rectangles
			// calculate the position of the ticks
			var nTick_px = nTop_px + nStep_px;
			aSmallTicks[nSmallTick_Idx++] = nTick_px;
		}
		// draw big ticks thicker and with a text label
		var nLargeTick_Idx=0;
		for ( var nStep_px = 0; nStep_px <= -nPixelRange; nStep_px += majorTickStep_px) {
			// calculate the position of the ticks
			var nTick_px = nTop_px + nStep_px;
			aLargeTicks[nLargeTick_Idx++] = nTick_px;
		}
	}
	// draw the small ticks
	for( var i = 0 ; i < aSmallTicks.length ;i++ )
	{
		ctx.fillStyle = sGray1;
		ctx.strokeStyle = sGray2;
	
		ctx.save();
		ctx.lineWidth = 1;
		ctx.beginPath();
		ctx.moveTo(nRight_px,aSmallTicks[i]+nTickThickness);
		ctx.lineTo(nRight_px+nTickWidth, aSmallTicks[i]+nTickThickness);
		ctx.lineTo(nRight_px+nTickWidth, aSmallTicks[i]-nTickThickness);
		ctx.lineTo(nRight_px, aSmallTicks[i]-nTickThickness);
		ctx.closePath();
		ctx.fill();
		ctx.stroke();
		ctx.restore();
	}
	// draw the large ticks and save labels for drawing at the end
	for( var i = 0 ; i < aLargeTicks.length ; i++ )
	{
		ctx.fillStyle = sGray1;
		ctx.strokeStyle = sGray2;

		ctx.save();
		ctx.lineWidth = 1;
		ctx.beginPath();
		ctx.moveTo(nRight_px,aLargeTicks[i]+nTickThickness*1.5);
		ctx.lineTo(nRight_px+nTickWidth*1.5, aLargeTicks[i]+nTickThickness*1.5);
		ctx.lineTo(nRight_px+nTickWidth*1.5, aLargeTicks[i]-nTickThickness*1.5);
		ctx.lineTo(nRight_px, aLargeTicks[i]-nTickThickness*1.5);
		ctx.closePath();
		ctx.fill();
		ctx.stroke();
		ctx.restore();

		// calc center position of text label
		x1 = nRight_px + nWidth*0.3;
		y1 = aLargeTicks[i];

		var nReadingForPixel= (aLargeTicks[i]-nBottom_px)/nPixelsPerDegree+nMin;
		
		sLabel = Math.floor(nReadingForPixel + 0.5) + '';
		var nLabelWidth = 15;
		if (ctx.measureText)
			nLabelWidth = ctx.measureText(sLabel).width;
		if (nLabelWidth <= nFontHeight)
			nLabelWidth = Math.max(1, sLabel.length) * nFontHeight * 0.7;
		aLabels.push({
			x : nRight_px + nTickWidth * 2,
			y : y1 + nFontHeight / 4,
			l : sLabel
		});
	}	
	
	// draw at bottom of gauge in 1 line
	if (ctx.measureText)
		aLabels.push({
			x : nLeft_px,
			y : nHeight - (1.5 * nFontHeight + 2),
			c : sTextColorForReading,
			l : sDisplay
		});

	// draw optional title at top left
	if( sTitle.length > 0 )
	{
		yFontStart = nFontHeight;
		if (ctx.measureText)
			aLabels.push({
				x : nLeft_px,
				y : yFontStart,
				c : sBlue,
				l : sTitle
			});
	}
	// draw all gauge labels last
	ctx.fillStyle = sBlack;
	ctx.strokeStyle = sBlack;
	for ( var i = 0; i < aLabels.length; i++) {
		ctx.save();
		ctx.lineWidth = 1;
		if (typeof (aLabels[i].c) != 'undefined') {
			ctx.strokeStyle = aLabels[i].c;
			ctx.fillStyle = aLabels[i].c;
		} else {
			ctx.strokeStyle = sBlack;
			ctx.fillStyle = sBlack;
		}

		if (typeof (aLabels[i].f) != 'undefined')
			ctx.font = aLabels[i].f;
		else
			ctx.font = sFont;

		ctx.translate(aLabels[i].x, aLabels[i].y);
		if (ctx.fillText)
			ctx.fillText(aLabels[i].l + '', 0, 0);
		ctx.restore();
	}

}