/*
FileName: raspiMower.cpp
This file is the main entry point for the program.
The program is written to run on a RaspberryPi Computer with a Raspberry Pi Camera.
It's primary purpose is to control a lawn mowing robot.


The MIT License (MIT)

Copyright (c) 2015 Ron Ostafichuk

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

#include <dirent.h> // for listing files in directory
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <stdint.h>
#include <unistd.h>
#include <linux/input.h>
#include <errno.h>

#include <string.h>
#include <time.h>
#include <wiringPi.h> // note: this program must run as sudo to be able to use the wiringPi library
#include <softPwm.h>

// For interprocess communication (ie. talking to the web page), use memcached [sudo apt-get install libmemcached-dev]
#include <libmemcached/memcached.h>

#include <ctime>
#include <fstream>
#include <iostream>

// for the raspicamera #include <raspicam/raspicam.h>

// for the image recognition and location algorithms
#include "locator.h"

Locator gLocator;

using namespace std;

// Defines for Physical Pin # to WiringPi Pin Number For Raspberry Pi B+ and Mk2
#define PhysPin3 8
#define PhysPin5 9
#define PhysPin7 7
#define PhysPin8 15
#define PhysPin10 16
#define PhysPin11 0
#define PhysPin12 1
#define PhysPin13 2
#define PhysPin15 3
#define PhysPin16 4
#define PhysPin18 5
#define PhysPin19 12
#define PhysPin21 13
#define PhysPin22 6
#define PhysPin23 14
#define PhysPin24 10
#define PhysPin26 11
#define PhysPin29 21
#define PhysPin31 22
#define PhysPin32 26
#define PhysPin33 23
#define PhysPin35 24
#define PhysPin36 27
#define PhysPin37 25
#define PhysPin38 28
#define PhysPin40 29


// set the Physical Pins for the functions they perform (j8Header PIN Numbers odd #=left side, even=right side)
#define WPP_OUT_UT_START PhysPin7 // Start a UT Pulse
#define WPP_IN_UT_ECHO PhysPin22 // Return Echo Pulse for UT Signal

#define WPP_OUT_RUNNING_LIGHTS PhysPin11
#define WPP_OUT_UT_Servo PhysPin12 // Hardware PWM Output to control the Servo that moves the Ultrasonic sensor

/* set the pins used for the DC MOTOR CONTROLLER */
#define WPP_OUT_RIGHT_MOTOR PhysPin13
#define WPP_OUT_RIGHT_MOTOR_PWM PhysPin15 // speed of motor once active

#define WPP_OUT_LEFT_MOTOR PhysPin16 // F=forward, B=backward , to activate motor, F must be HIGH and B must be LOW or vise-versa ...
#define WPP_OUT_LEFT_MOTOR_PWM PhysPin18 /* speed of motor once active */

/* Misc digital outputs */
#define WPP_OUT_HEAD_LIGHT PhysPin29
#define WPP_OUT_MOWER PhysPin31
#define WPP_OUT_NERF PhysPin32

/* Bump Sensors around perimiter of robot */
#define WPP_IN_BUMP_FRONT_RIGHT PhysPin37
#define WPP_IN_BUMP_FRONT_LEFT PhysPin38
#define WPP_IN_BUMP_REAR PhysPin40



enum MotorState {eMS_Stopped=0,eMS_Forward,eMS_Backward,eMS_Left_Turn,eMS_Right_Turn,eMS_Left_Pivot,eMS_Right_Pivot} gMotorState;

/* Use the web interface to override the operation of the robot */
struct timespec gStartTime; // used to reduce the times to deltas from start time of program
long gTimeManualOverride_ms = 0; // in milli-seconds
long gTimeSoundStart_ms = 0; // in milli-seconds , when was the last sound request made, we don't allow another for 2-3? seconds
long gTimeNow_ms=0; // keep the current time in ms here always
long gTimeShootStart_ms=0; // time for shoot start, must turn off after 0.25 seconds
int gNumSoundFiles=-1; // number of sound files to randomly select from

int gUserJoystickX = 0; // web gui joystick values (-100 to 100) if user is controlling via web interface
int gUserJoystickY = 0;

int gMotorMaxPWM = 30; // for development I want to prevent 100% speed on motors, keep it slow to prevent injuries!
int gMotorLeftPWM = 0; // Current left motor speed setting ok values are (0-100)
int gMotorRightPWM = 0; // Current left motor speed setting ok values are (0-100)

long gTimeMotorStart_ms=0; // Time when the motor was activated, used for controlling ramp

memcached_st *gMC = NULL; // global pointer to memcached for communication with web server

const char *mc_config_string= "--SERVER=localhost";
const char *keyUserCommand = "userCommand";
const char *keyUserCommand_ms = "userCommand_ms";
const char *keyPrevUserCommand = "prevUserCommand";

const char *keyTextToSpeak = "userText";

const char *keyMowerStatus = "mowerStatus";
const char *keyRunningLightsStatus = "runningLightsStatus";
const char *keyHeadLightsStatus = "headLightsStatus";
const char *keyMotionStatus = "motionStatus";

const char *keyLeftMotorPWM = "LeftMotorPWM";
const char *keyRightMotorPWM = "RightMotorPWM";


// for high res control of motors from web interface we need 3 variables
const char *keyJoystick_ms = "joystick_ms";
const char *keyJoystickX = "joystickX";
const char *keyJoystickY = "joystickY";

// flags to hold the state of the relays for the lights and mower
int gnRunningLights = 0;
int gnHeadLights = 0;
int gnMowerRunning = 0;


// function for getting the time in ms
long getTime_ms()
{
	long time_ms;
	struct timespec t;
	clock_gettime(CLOCK_REALTIME, &t);

    // do not want time since epoch (overflow!), instead we want time since 2015-01-01 to prevent overflow
	//time_ms = (t.tv_sec-gStartTime.tv_sec)*1000 + (t.tv_nsec / 1000000); // Convert nanoseconds to milliseconds
	time_ms = (t.tv_sec-1420070400)*1000 + (t.tv_nsec / 1000000); // Convert nanoseconds to milliseconds
	return time_ms;
}

void beginSoftStart()
{
    // set the starting PWM to 1% and the starting time to now so we can do a softstart on the motors when using dumb controls
    gMotorLeftPWM = 0;
    gMotorRightPWM = 0;
    gTimeMotorStart_ms = getTime_ms();
}

void motorRampUp()
{
    // continuously ramp up speed over 0.5 seconds until gMotorMaxPWM is reached
    if( gUserJoystickX == 0 && gUserJoystickY == 0 )
    {
        // crude motor control using buttons
        if( (gMotorState == eMS_Forward || gMotorState == eMS_Right_Pivot || gMotorState == eMS_Right_Turn )
           && gMotorLeftPWM != gMotorMaxPWM )
        {
            int nSpeed = gMotorMaxPWM*(gTimeNow_ms - gTimeMotorStart_ms)/500 + 1; // calculate speed ramp over 1/2 second
            if( nSpeed < 0 )
                nSpeed = 0;
            if( nSpeed > gMotorMaxPWM )
                nSpeed = gMotorMaxPWM;
            if( gMotorLeftPWM != nSpeed )
                fprintf(stdout, "Left MOTOR PWM: %i\r\n",nSpeed);
            gMotorLeftPWM = nSpeed;
        }
        if( (gMotorState == eMS_Backward || gMotorState == eMS_Left_Pivot )
           && gMotorLeftPWM != -gMotorMaxPWM )
        {
            int nSpeed = -gMotorMaxPWM*(gTimeNow_ms - gTimeMotorStart_ms)/500 + 1; // calculate speed ramp over 1/2 second
            if( nSpeed > 0 )
                nSpeed = 0;
            if( nSpeed < -gMotorMaxPWM )
                nSpeed = -gMotorMaxPWM;
            if( gMotorLeftPWM != nSpeed )
                fprintf(stdout, "Left MOTOR PWM: %i\r\n",nSpeed);
            gMotorLeftPWM = nSpeed;
        }

        if( (gMotorState == eMS_Forward || gMotorState == eMS_Left_Pivot || gMotorState == eMS_Left_Turn )
           && gMotorRightPWM != gMotorMaxPWM )
        {
            int nSpeed = gMotorMaxPWM*(gTimeNow_ms - gTimeMotorStart_ms)/500 + 1; // calculate speed ramp over 1/2 second
            if( nSpeed < 0 )
                nSpeed = 0;
            if( nSpeed > gMotorMaxPWM )
                nSpeed = gMotorMaxPWM;
            if( gMotorLeftPWM != nSpeed )
                fprintf(stdout, "Right MOTOR PWM: %i\r\n",nSpeed);
            gMotorRightPWM = nSpeed;
        }
        if( (gMotorState == eMS_Backward || gMotorState == eMS_Right_Pivot )
           && gMotorRightPWM != -gMotorMaxPWM )
        {
            int nSpeed = -gMotorMaxPWM*(gTimeNow_ms - gTimeMotorStart_ms)/500 + 1; // calculate speed ramp over 1/2 second
            if( nSpeed > 0 )
                nSpeed = 0;
            if( nSpeed < -gMotorMaxPWM )
                nSpeed = -gMotorMaxPWM;
            if( gMotorLeftPWM != nSpeed )
                fprintf(stdout, "Right MOTOR PWM: %i\r\n",nSpeed);
            gMotorRightPWM = nSpeed;
        }

        softPwmWrite (WPP_OUT_LEFT_MOTOR_PWM, abs(gMotorLeftPWM)) ;
        softPwmWrite (WPP_OUT_RIGHT_MOTOR_PWM, abs(gMotorRightPWM)) ;
    }
}

// Motor Control functions
void motorsStop()
{
	digitalWrite(WPP_OUT_LEFT_MOTOR,LOW);
	digitalWrite(WPP_OUT_RIGHT_MOTOR,LOW);
	if( gMotorState != eMS_Stopped )
		fprintf(stdout, "MOTORS: STOP!\r\n");
	gMotorState = eMS_Stopped;

    gMotorLeftPWM = 0; // set to zero to stop motor
    gMotorRightPWM = 0;
    gTimeMotorStart_ms = 0;

	char c[10] = "Stopped";
	memcached_set(gMC, keyMotionStatus, strlen(keyMotionStatus), &c[0], strlen(c), 0, 0);
}
void motorsForward()
{
	/* Both Motors forward */
	digitalWrite(WPP_OUT_LEFT_MOTOR,HIGH);
	digitalWrite(WPP_OUT_RIGHT_MOTOR,HIGH);

	if( gMotorState != eMS_Forward )
		fprintf(stdout, "MOTORS: Forward\r\n");
    gMotorState = eMS_Forward;
    if( gMotorState == eMS_Stopped )
    {
        beginSoftStart();
    }

	char c[10] = "Forwards";
	memcached_set(gMC, keyMotionStatus, strlen(keyMotionStatus), c, strlen(c), 0, 0);
}
void motorsBackward()
{
	digitalWrite(WPP_OUT_LEFT_MOTOR,LOW);
	digitalWrite(WPP_OUT_RIGHT_MOTOR,LOW);

	if( gMotorState != eMS_Backward )
		fprintf(stdout, "MOTORS: Backward\r\n");
    gMotorState = eMS_Backward;
    if( gMotorState == eMS_Stopped )
    {
        beginSoftStart();
    }

	char c[10] = "Backwards";
	memcached_set(gMC, keyMotionStatus, strlen(keyMotionStatus), &c[0], strlen(c), 0, 0);
}

void motorsLeftTurn()
{
	digitalWrite(WPP_OUT_LEFT_MOTOR,LOW);
	digitalWrite(WPP_OUT_RIGHT_MOTOR,LOW);
	if( gMotorState != eMS_Left_Turn )
		fprintf(stdout, "MOTORS: Left Turn\r\n");

	gMotorState = eMS_Left_Turn;
    if( gMotorState == eMS_Stopped )
    {
        beginSoftStart();
    }

	char c[15] = "Left Turn";
	memcached_set(gMC, keyMotionStatus, strlen(keyMotionStatus), &c[0], strlen(c), 0, 0);
}
void motorsRightTurn()
{
	digitalWrite(WPP_OUT_LEFT_MOTOR,HIGH);
	digitalWrite(WPP_OUT_RIGHT_MOTOR,LOW);
	if( gMotorState != eMS_Right_Turn )
		fprintf(stdout, "MOTORS: Right Turn\r\n");
	gMotorState = eMS_Right_Turn;
    if( gMotorState == eMS_Stopped )
    {
        beginSoftStart();
    }

	char c[15] = "Right Turn";
	memcached_set(gMC, keyMotionStatus, strlen(keyMotionStatus), &c[0], strlen(c), 0, 0);
}
void motorsLeftPivot()
{
	digitalWrite(WPP_OUT_LEFT_MOTOR,LOW);
	digitalWrite(WPP_OUT_RIGHT_MOTOR,HIGH);
	if( gMotorState != eMS_Left_Pivot )
		fprintf(stdout, "MOTORS: Pivot Left\r\n");
	gMotorState = eMS_Left_Pivot;
    if( gMotorState == eMS_Stopped )
    {
        beginSoftStart();
    }

	char c[15] = "Pivot Left";
	memcached_set(gMC, keyMotionStatus, strlen(keyMotionStatus), &c[0], strlen(c), 0, 0);
}
void motorsRightPivot()
{
	digitalWrite(WPP_OUT_LEFT_MOTOR,HIGH);
	digitalWrite(WPP_OUT_RIGHT_MOTOR,LOW);

	if( gMotorState != eMS_Right_Pivot )
		fprintf(stdout, "MOTORS: Pivot Right\r\n");
	gMotorState = eMS_Right_Pivot;
    if( gMotorState == eMS_Stopped )
    {
        beginSoftStart();
    }

	char c[15] = "Pivot Right";
	memcached_set(gMC, keyMotionStatus, strlen(keyMotionStatus), &c[0], strlen(c), 0, 0);
}

void mower(int n)
{
	if( n >0)
	{
		digitalWrite(WPP_OUT_MOWER,HIGH);
		if( gnMowerRunning != 1 )
			fprintf(stdout, "Mower: ENGAGED! WATCH YER FINGERS!!!\r\n");
		gnMowerRunning = 1;

        char c[5] = "On";
        memcached_set(gMC, keyMowerStatus, strlen(keyMowerStatus), &c[0], strlen(c), 0, 0);
	} else
	{
		digitalWrite(WPP_OUT_MOWER,LOW);
		if( gnMowerRunning != 0 )
			fprintf(stdout, "Mower: stopped.\r\n");
		gnMowerRunning = 0;

        char c[5] = "Off";
        memcached_set(gMC, keyMowerStatus, strlen(keyMowerStatus), &c[0], strlen(c), 0, 0);
	}
}

void shoot(int n)
{
    // shoot the gun by triggering the digital output for 0.25 seconds
    if( n > 0 )
    {
        // start the output signal
        digitalWrite(WPP_OUT_NERF,HIGH);
        gTimeShootStart_ms = gTimeNow_ms; // set new start time for shoot request
    } else
    {
        digitalWrite(WPP_OUT_NERF,LOW);
        gTimeShootStart_ms = 0; // clear
    }
}


void runningLights(int n)
{
	if( n> 0 )
	{
		digitalWrite(WPP_OUT_RUNNING_LIGHTS,HIGH);
		if( gnRunningLights != 1 )
			fprintf(stdout, "Running Lights: ON\r\n");
		gnRunningLights = 1;

        char c[5] = "On";
        memcached_set(gMC, keyRunningLightsStatus, strlen(keyRunningLightsStatus), &c[0], strlen(c), 0, 0);
	} else
	{
		digitalWrite(WPP_OUT_RUNNING_LIGHTS,LOW);
		if( gnRunningLights != 0 )
			fprintf(stdout, "Running Lights: OFF\r\n");
		gnRunningLights = 0;

        char c[5] = "Off";
        memcached_set(gMC, keyRunningLightsStatus, strlen(keyRunningLightsStatus), &c[0], strlen(c), 0, 0);
	}
}

void headLights(int n)
{
	if(n>0)
	{
		digitalWrite(WPP_OUT_HEAD_LIGHT,HIGH);
		if( gnHeadLights != 1 )
			fprintf(stdout, "Head Lights: ON\r\n");
		gnHeadLights = 1;

        char c[5] = "On";
        memcached_set(gMC, keyHeadLightsStatus, strlen(keyHeadLightsStatus), &c[0], strlen(c), 0, 0);
	} else
	{
		digitalWrite(WPP_OUT_HEAD_LIGHT,LOW);
		if( gnHeadLights != 0 )
			fprintf(stdout, "Head Lights: OFF\r\n");
		gnHeadLights = 0;

        char c[5] = "Off";
        memcached_set(gMC, keyHeadLightsStatus, strlen(keyHeadLightsStatus), &c[0], strlen(c), 0, 0);
	}
}

void playSound(char cFileName[500] )
{
	if( gTimeSoundStart_ms < gTimeNow_ms - 3000 )
	{
	    char cCommand[800];
		// ok to play another sound
		gTimeSoundStart_ms = gTimeNow_ms; // set new start time for sound request
		// play the sound in another processor
		strcpy(cCommand,"omxplayer ");
		strcat (cCommand,cFileName);
		strcat (cCommand," &"); // tells command to run as a seperate process and return immediately
		system(cCommand);
		fprintf(stdout, "Sound: %s\r\n",cFileName);
	}
}

void speak(char cSentence[500] )
{
    if( gTimeSoundStart_ms < gTimeNow_ms - 3000 )
    {
        char cCommand[800];
        gTimeSoundStart_ms = gTimeNow_ms; // set new start time for sound request
        // use espeak to say a sentence
        strcpy(cCommand,"espeak -a 200 -p 80 -s 100 -k10 -v+croak \"");
        strcat (cCommand,cSentence);
        strcat (cCommand,"\" &"); // tells command to run as a seperate process and return immediately
        system(cCommand);
        fprintf(stdout, "Speak: %s\r\n",cSentence);
    }
}

void clearLastCmd()
{
    // clear the memcached command so it does not get repeated
    char c = '\0';
    memcached_set(gMC, keyUserCommand_ms, strlen(keyUserCommand_ms), &c, 1, 0, 0);
}

void ProcessCommands(char *cmd)
{
    /* Process command strings sent from the web user interface through memcached */
    /* control the motors */
    if( strcasecmp(cmd,"Stop") == 0 )
    {
        motorsStop(); // most important command!
    } else if( strcasecmp(cmd,"Move Forward") == 0 )
    {
        motorsForward();
    } else if( strcmp(cmd,"Turn Left") == 0 )
    {
        motorsLeftTurn();
    } else if( strcmp(cmd,"Turn Right") == 0)
    {
        motorsRightTurn();
    } else if( strcmp(cmd,"Pivot Left") == 0 )
    {
        motorsLeftPivot();
    } else if( strcmp(cmd,"Pivot Right") == 0)
    {
        motorsRightPivot();
    } else if( strcmp(cmd,"Move Backward") == 0)
    {
        motorsBackward();
    } else if( strcmp(cmd,"Mower On") == 0 )
    {
        mower(1);
    } else if( strcmp(cmd,"Mower Off") == 0)
    {
        mower(0);
    } else if( strcmp(cmd,"Shoot") == 0 )
    {
        shoot(1); // fire gun for 0.25 seconds
    } else if( strcmp(cmd,"Running Lights On") == 0 )
    {
        runningLights(1);
    } else if( strcmp(cmd,"Running Lights Off") == 0)
    {
        runningLights(0);
    } else if( strcmp(cmd,"Head Lights On") == 0 )
    {
        headLights(1);
    } else if( strcmp(cmd,"Head Lights Off") == 0 )
    {
        headLights(0);
    } else if( strcmp(cmd,"Play Exterminate") == 0 )
    {
        char cFileName[200];
        strncpy(cFileName,"sounds/Exterminate.mp3",200);
        playSound(cFileName); // returns immediately
    } else if( strcmp(cmd,"Play Destroy") == 0 )
    {
        char cFileName[200];
        strncpy(cFileName,"sounds/Destroy.mp3",200);
        playSound(cFileName); // returns immediately
    } else if( strcmp(cmd,"Play Random") == 0 )
    {
        // random sound from the directory of available sounds
        char cFileName[200];
        DIR           *d;
        struct dirent *dir;
        d = opendir("./sounds");

        if (d)
        {
            if( gNumSoundFiles == -1 )
            {
                // count the number of sound files for the randomizer
                gNumSoundFiles =0;
                while ((dir = readdir(d)) != NULL)
                {
                    if( !(strcmp(dir->d_name,".") == 0 || strcmp(dir->d_name,"..")==0 ))
                    {
                        gNumSoundFiles += 1;
                    }
                }
                closedir(d);
                cout << "Found " << gNumSoundFiles << " Sounds Files for random playing" << std::endl;
                d = opendir("./sounds");
            }
            int nRand = rand() % gNumSoundFiles; // random audio clip selection
            int nFileNum = 0;
            while ((dir = readdir(d)) != NULL)
            {
                if( !(strcmp(dir->d_name,".") == 0 || strcmp(dir->d_name,"..")==0 ))
                {
                    strncpy(cFileName,"sounds/",200);
                    strcat(cFileName,dir->d_name);
                    if( nRand == nFileNum )
                    {
                        // play whatever is in the cFileName
                        if( strlen(cFileName) > 0 )
                            playSound(cFileName);
                        break; // exit loop
                    }
                    nFileNum += 1;
                }
            }
            closedir(d);
        }
    } else if( strcmp(cmd,"Start Patrol") == 0 )
    {
        speak((char *)"Yes Master I will Begin Security Patrols");
    }
    else if( strcmp(cmd,"Start Locate") == 0 )
    {
        speak((char *)"Yes Master I will run my navigation routines");
    }
    else if( strcmp(cmd,"Start Mowing") == 0 )
    {
        speak((char *)"Yes Master I will Mow your lawn");
    }
    else if( strcmp(cmd,"Start Park") == 0 )
    {
        speak((char *)"Yes Master I will Park");
    }
    else if( strcmp(cmd,"Speak") == 0 )
    {
		size_t value_length=0;
		uint32_t flags=0;
		memcached_return_t error;

        char * pTextToSpeak = memcached_get(gMC, keyTextToSpeak, strlen(keyTextToSpeak), &value_length, &flags, &error);
        if( pTextToSpeak != NULL )
        {
            speak(pTextToSpeak);
        } else
        {
            speak((char *)"Yes Master, what should I say?");
        }
    }
    else if( strlen(cmd)>0 ) // any undefined command
    {
        // all other commands stop the motor!
        motorsStop();
        cout << "Unrecognized Command" << endl;
        speak((char *)"Sorry Master you have issued an unrecognized command");
    }
}

void eventProcessor()
{
	// This is the main loop that processes all inputs
	int nError =0;
    char cPreviousCmd[200]; // used to tell when a change in user commands has occurred

	fprintf(stdout, "Started eventProcessing\r\n");

	while( nError == 0 )
	{
	    // reduce CPU load by sleeping for 25 milliseconds each loop
        struct timespec tim, tim2;
        tim.tv_sec = 0;
        tim.tv_nsec = 25000;

        if(nanosleep(&tim , &tim2) < 0 )
        {
            printf("Nano sleep system call failed \n");
        }

		/* handle all logic states here, only return on serious error */
		gTimeNow_ms = getTime_ms();

        // check if digital output needs to stop
        if( gTimeShootStart_ms != 0 && gTimeShootStart_ms < gTimeNow_ms - 250 )
        {
            // stop the shot
            shoot(0);
        }


		// get latest commands
		size_t value_length;
		uint32_t flags;
		memcached_return_t error;

		char * cmd = memcached_get(gMC, keyUserCommand, strlen(keyUserCommand), &value_length, &flags, &error);
		if( cmd != NULL )
		{
		    // valid command found, check if it is current
		    char * char_cmd_ms = memcached_get(gMC,keyUserCommand_ms,strlen(keyUserCommand_ms), &value_length, &flags, &error );
		    long nTimeSinceIssue = 99999;
		    if( char_cmd_ms != NULL )
		    {
		        long long cmd_ms = strtol(char_cmd_ms,NULL,10);
		        nTimeSinceIssue = gTimeNow_ms - cmd_ms;
		    }
		    if( nTimeSinceIssue > 2000 )
		    {
		        // command is over 2 seconds old, ignore it by writing an end of string char in the memory!
		        strncpy(cmd,"\0",1);
		    }

			if( strlen(cmd)>0 )
			{
			    if( strcasecmp(cmd,cPreviousCmd) != 0 )
                    cout << "Received Command " << cmd << endl;

				if( gTimeManualOverride_ms == 0 )
				{
					/* output notice of state change */
					cout << "Manual Override Started!" << endl << "================" << endl;
				}
				gTimeManualOverride_ms = gTimeNow_ms; /* every user command resets timer */
			}

            if( cmd != NULL && strcmp(cmd,"End Program") == 0 )
            {
                motorsStop(); // most important command!
                cout << "Program terminated by user" << endl;
                return; // end program
            }
		}


        // get x and y joystick values, if they are within 250ms of now
        int x=0,y=0;
        char * pJoy_ms = memcached_get(gMC, keyJoystick_ms, strlen(keyJoystick_ms), &value_length, &flags, &error);
        if( pJoy_ms != NULL )
        {
            // check if current
            long nTimeSinceEvent_ms = 99999;
            {
                long long joy_ms = strtol(pJoy_ms,NULL,10);
                nTimeSinceEvent_ms = gTimeNow_ms - joy_ms;
            }
            if( nTimeSinceEvent_ms < 2000 )
            {
                // is current, ok to control motors!
                char * pJoyX = memcached_get(gMC, keyJoystickX, strlen(keyJoystickX), &value_length, &flags, &error);
                if( pJoyX != NULL )
                {
                    x = strtol(pJoyX,NULL,10);
                }
                char * pJoyY = memcached_get(gMC, keyJoystickY, strlen(keyJoystickY), &value_length, &flags, &error);
                if( pJoyY != NULL )
                {
                    y = strtol(pJoyY,NULL,10);
                }
                char c[35] = "Joystick Controlled";
                memcached_set(gMC, keyMotionStatus, strlen(keyMotionStatus), &c[0], strlen(c), 0, 0);

                // check if we are in automatic mode
                if( gTimeManualOverride_ms == 0 )
                {
                    /* output notice of state change */
                    cout << "Manual Override Started!" << endl << "================" << endl;
                }
                gTimeManualOverride_ms = gTimeNow_ms; /* every joystick movement resets timer */
            }
        }

        if( gTimeManualOverride_ms > 0 )
        {
            // high res motor control using x,y joystick (if in manual override mode)
            // x = difference between left and right motor speed
            // y = overall forward or backward speed

            // limit to +-100 for joystick values
            if( y > 100 ) y = 100;
            if( y < -100 ) y = -100;
            if( x > 100 ) x = 100;
            if( x < -100 ) x = -100;

            // driving forward or backward , set magnitude
            gMotorLeftPWM = -y * gMotorMaxPWM/2/100 + x*gMotorMaxPWM/2/100; // y is primary speed control
            gMotorRightPWM = -y * gMotorMaxPWM/2/100 - x*gMotorMaxPWM/2/100;

            // set the motor Direction
            if( gMotorLeftPWM > 0 )
                digitalWrite(WPP_OUT_LEFT_MOTOR,HIGH);
            else
                digitalWrite(WPP_OUT_LEFT_MOTOR,LOW);

            if( gMotorRightPWM > 0 )
                digitalWrite(WPP_OUT_RIGHT_MOTOR,HIGH);
            else
                digitalWrite(WPP_OUT_RIGHT_MOTOR,LOW);

            // set the motor PWM
            softPwmWrite (WPP_OUT_LEFT_MOTOR_PWM, abs(gMotorLeftPWM)) ;
            softPwmWrite (WPP_OUT_RIGHT_MOTOR_PWM, abs(gMotorRightPWM)) ;
        }

		if( gTimeManualOverride_ms > 0 && cmd != NULL && strlen(cmd)>0 )
		{
		    // MANUAL OVERRIDE LOGIC HAPPENS HERE
		    ProcessCommands(cmd);
			// save previous command for reference
            strncpy(cPreviousCmd,cmd,200);
            if( strlen(cPreviousCmd) > 0 )
            {
                memcached_set(gMC, keyPrevUserCommand, strlen(keyPrevUserCommand), &cPreviousCmd[0], strlen(cPreviousCmd), 0, 0);
            }
            clearLastCmd();
		}

        if( gTimeManualOverride_ms > 0 && (gTimeNow_ms - gTimeManualOverride_ms) > 1999 )
        {
            motorsStop(); /* only allow 2 second of movement per MANUAL command (used to overcome key repeat delay, and for safety) */
        }

		/* check for 30 minutes since last Manual override to return to automatic mode or the 'r' key pressed */
		if( (cmd!=NULL && strcmp(cmd,"Automatic Mode")==0) || (gTimeManualOverride_ms > 0 && (gTimeNow_ms - gTimeManualOverride_ms) > 30*60*1000 ) )
		{
			/* return to automatic control */
			motorsStop();
			fprintf(stdout, "Automatic Control Restored\r\n========================\r\n");
			gTimeManualOverride_ms = 0;
		}

        if( gMotorState != eMS_Stopped )
        {
            motorRampUp(); // handle motor speed ramp if motor is running
        }

        // save the current PWM value for each motor in memcached for status display on web page and debugging
        char c [33];
        sprintf(c,"%i",gMotorLeftPWM);
        memcached_set(gMC, keyLeftMotorPWM, strlen(keyLeftMotorPWM), &c[0], strlen(c), 0, 0);
        sprintf(c,"%i",gMotorRightPWM);
        memcached_set(gMC, keyRightMotorPWM, strlen(keyRightMotorPWM), &c[0], strlen(c), 0, 0);
	}

	return;
}


/**********************************************
* main entry point for the RaspiMower program *
***********************************************/
int main (void)
{
   	clock_gettime(CLOCK_REALTIME, &gStartTime); // get start time for program to make handling of time fit within a long

	printf("RaspiMower:\r\n");
	printf("Movement Commands: Move Forward, Turn Left, Turn Right, Pivot Left, Pivot Right, Move Backwards, Stop Motors\r\n");
    printf("Alternate Movement Commands if using speed controller: Joystick(0,0), Stop Motors\r\n");

	printf("Auxiliary Commands: Running Lights On, Running Lights Off, Head Lights On, Head Lights Off, Fire Nerfgun\r\n");

	printf("Sounds: Play EXTERMINATE, Play OBEY, Play Random");

	printf("Mower:  Mower On, Mower Off\r\n");
	printf("\r\n");
	printf("Automatic Modes: Start Patrol, Start Mowing, Start Locate, Start Park");
	printf("Terminate: Exit, Terminate\r\n");

	printf("any undefined command will Stop Motors\r\n");
	printf("===========================\r\n");
	std::cout << "Delta Time_ms=" << getTime_ms() << std::endl;

	// get connection to memcached so we can talk to web page easily and provide continuous but temporary
	// low res images to web server without wearing out the SD card (Also SD card access is extremely slow compared to memory)
   gMC= memcached(mc_config_string, strlen(mc_config_string));

   if( gMC == NULL )
   {
		fprintf(stdout,"Error connecting to MemCache Localhost (Needed for web page interaction)\r\n");
		return 0;
   }

	//InitCamera(); // call this after the sound is played because there is a 3 second wait for the camera to stabilize

	/* initialize the wiringPi library */
	wiringPiSetup();

	/* Set the pin modes for all pins, very important to get inputs and outputs correct or you can fry the board!!! */
	/****************************************************************************************************************/

	/* Set the pin modes for the DC motor controller */
	pinMode(WPP_OUT_LEFT_MOTOR,OUTPUT);
	pinMode(WPP_OUT_RIGHT_MOTOR,OUTPUT);

	/* convert the motor pins to software PWM pins */
    if( softPwmCreate (WPP_OUT_LEFT_MOTOR_PWM, 0, 100) != 0 )
        fprintf(stdout,"Error, unable to set Pin WPP_OUT_LEFT_MOTOR_PWM to PWM, will run at 100 ALWAYS\r\n");
    if( softPwmCreate (WPP_OUT_RIGHT_MOTOR_PWM, 0, 100) != 0 )
        fprintf(stdout,"Error, unable to set Pin WPP_OUT_LEFT_MOTOR_PWM to PWM, will run at 100 ALWAYS\r\n");
    /* use void softPwmWrite (int pin, int value) ; to set PWM */
    // default speed is 0 % for stopped
    softPwmWrite (WPP_OUT_LEFT_MOTOR_PWM, 0) ;
    softPwmWrite (WPP_OUT_RIGHT_MOTOR_PWM, 0) ;


    pinMode(WPP_OUT_UT_START,OUTPUT);
    pinMode(WPP_IN_UT_ECHO,INPUT);

	pinMode(WPP_OUT_MOWER,OUTPUT);
	pinMode(WPP_OUT_HEAD_LIGHT,OUTPUT);
	pinMode(WPP_OUT_RUNNING_LIGHTS,OUTPUT);
    pinMode(WPP_OUT_NERF,OUTPUT);


    pinMode(WPP_OUT_UT_Servo,PWM_OUTPUT);
    pwmWrite(WPP_OUT_UT_Servo,0); // Move servo to starting position, default range is 1024

    /* Bump Sensors around perimiter of robot */
    pinMode(WPP_IN_BUMP_FRONT_RIGHT,INPUT);
    pinMode(WPP_IN_BUMP_FRONT_LEFT,INPUT);
    pinMode(WPP_IN_BUMP_REAR,INPUT);


	/* make sure motors are stopped immediately in case the program is recovering from a crash */
	motorsStop();


  // play startup sound after the init , because if init fails then we do not want the sound to play
	system("omxplayer sounds/Emerge.mp3 &"); // do not wait for program to finish

	/* now that the pins are ready and the motors are stopped, enter the main event loop */
	eventProcessor();

	/* if the event processor returns, then set all the DC Controller pins to LOW to make sure the motors are stopped*/
	motorsStop();

    memcached_free(gMC); // free up memcached connection

	system("omxplayer sounds/Survive_in_Me.mp3 &"); // do not wait for program to finish
	fprintf(stdout,"Program Quit\r\n");
	return 0;
}

